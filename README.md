# init-race-condition

A fairly common pattern for an enumeration type (instead of using Scala's enumerations, which aren't ideal) is to use a trait or an 
abstract class, then define case classes/objects that extend the trait or abstract class. These definitions are often collected together
in a companion object.

Also typically, a collection of the enumerated instances is provided. This is useful for, eg, looking up by string value. 

An example:

```scala
sealed abstract class MyEnumType(val name: String)

object MyEnumType {

  final case object FirstEnum extends MyEnumType("first")
  final case object SecondEnum extends MyEnumType("second")

  val values = Set(
    FirstEnum,
    SecondEnum
  )
  
  def findByName(name: String): Option[MyEnumType] = {
    values.find(_.name == name)
  }
}

```

Things get interesting, however, if the abstract class has a constructor parameter with a default value, eg:

```scala
sealed abstract class MyEnumType(val name: String, val optional: String = "")

object MyEnumType {

  final case object FirstEnum extends MyEnumType("first") // use default value
  final case object SecondEnum extends MyEnumType("second", "optional2")

  val values = Set(
    FirstEnum,
    SecondEnum
  )
  
  def findByName(name: String): Option[MyEnumType] = {
    values.find(_.name == name)
  }
}

```

In this case, the following test code passes:

```scala
  val _ = MyEnumType.SecondEnum
  MyEnumType.values.contains(null) must be(false)
```

whereas the following fails:

```scala
  val _ = MyEnumType.FirstEnum
  MyEnumType.values.contains(null) must be(false)
```

Note, if the `values` collection is accessed first, there are never any null values in the collection.

For a guess, there's some kind of race condition initialising the class and the companion object and it's fields, although it's not
clear why the presence of a default constructor parameter would affect the behaviour in this way, or why first accessing a case object
that supplies the optional parameter is ok, whereas first accessing a case object that doesn't is not!

### Working examples

This project contains examples of the above enumeration patterns, with corresponding tests, some of which fail, as above.

Note, each test is run in a separate, forked JVM, so that the test completely controls the order of initialisation of classes, etc.
Run the tests in a shell, rather than in an IDE.

This has been run against various versions of Scala from 2.11.0 to 2.13.1, and Oracle's JDK 8 and 11, all with the same results.

### The solution

Make the `values` collection lazy.

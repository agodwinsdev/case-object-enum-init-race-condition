import Tests._

name := "init-race-condition"

organization := "uk.co.sdev"

version := "0.0.1-SNAPSHOT"

scalaVersion := "2.13.1"

scalacOptions ++= Seq(
  "-encoding", "UTF-8",
  "-deprecation",
  "-unchecked",
  "-feature",
  "-Xlint",
  "-Xfatal-warnings"
)

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.0.8" % "test"
)

def forkedJvmPerTest(testDefs: Seq[TestDefinition]): Seq[Group] = {
  testDefs map { test =>
    Group(
      name = test.name,
      tests = Seq(test),
      runPolicy = SubProcess(ForkOptions())
    )
  }
}

testGrouping in Test := forkedJvmPerTest((definedTests in Test).value)

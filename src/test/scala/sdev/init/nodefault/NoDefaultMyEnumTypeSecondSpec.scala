package sdev.init.nodefault

import org.scalatest.{MustMatchers, WordSpec}

class NoDefaultMyEnumTypeSecondSpec extends WordSpec with MustMatchers {

  "Enum values" should {
    "not contain null value if first object accessed is second enum object" in {
      val _ = MyEnumType.SecondEnum
      MyEnumType.values.contains(null) must be(false)
    }
  }
}

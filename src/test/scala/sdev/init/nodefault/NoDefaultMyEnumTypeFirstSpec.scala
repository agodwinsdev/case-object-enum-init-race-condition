package sdev.init.nodefault

import org.scalatest.{MustMatchers, WordSpec}

class NoDefaultMyEnumTypeFirstSpec extends WordSpec with MustMatchers {

  "Enum values" should {
    "not contain null value if first object accessed is first enum object" in {
      val _ = MyEnumType.FirstEnum
      MyEnumType.values.contains(null) must be(false)
    }
  }
}

/**
  * Copyright 2019 Andy Godwin
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package sdev.init.nodefault

import org.scalatest.{MustMatchers, WordSpec}

class NoDefaultMyEnumTypeValuesThenSecondSpec extends WordSpec with MustMatchers {

  "Enum values" should {
    "not contain null value if values is accessed first followed by second enum" in {
      val values = MyEnumType.values
      val _ = MyEnumType.SecondEnum
      values.contains(null) must be(false)
    }
  }
}


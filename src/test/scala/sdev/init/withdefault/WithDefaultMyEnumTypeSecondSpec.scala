package sdev.init.withdefault

import org.scalatest.{MustMatchers, WordSpec}

class WithDefaultMyEnumTypeSecondSpec extends WordSpec with MustMatchers {

  "Enum values" should {
    "not contain null value if first object accessed is second defined and provides optional value" in {
      val _ = MyEnumType.SecondEnum
      MyEnumType.values.contains(null) must be(false)
    }
  }
}

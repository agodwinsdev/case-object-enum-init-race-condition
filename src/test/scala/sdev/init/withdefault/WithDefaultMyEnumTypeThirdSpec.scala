package sdev.init.withdefault

import org.scalatest.{MustMatchers, WordSpec}

class WithDefaultMyEnumTypeThirdSpec extends WordSpec with MustMatchers {

  "Enum values" should {
    "not contain null value if first object accessed is third defined and uses default parameter" in {
      val _ = MyEnumType.ThirdEnum
      MyEnumType.values.contains(null) must be(false)
    }
  }
}

package sdev.init.withdefault

import org.scalatest.{MustMatchers, WordSpec}

class WithDefaultMyEnumTypeFirstSpec extends WordSpec with MustMatchers {

  "Enum values" should {
    "not contain null value if first object accessed is first defined and uses default parameter" in {
      val _ = MyEnumType.FirstEnum
      MyEnumType.values.contains(null) must be(false)
    }
  }
}
